== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Benítez Galván, Salvador
* Casermeiro Pérez, Paula
* Cid Medina, Rafael
* Contreras Fernández, Julio
* Luque Giráldez, José Rafael
* Zea Diez, Jaime

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

=== Fiat Multipla (5 plazas)

* Salvador Benitez

==== Peugeot 508 (5 plazas)

* Rafael Luque

==== VolksWagen Passat (5 plazas)

* Julio Contreras
* Rafael Cid

==== Audi A4 (5 plazas)

* Paula Casermeiro
* Jaime Zea Diez
